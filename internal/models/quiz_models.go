package models

import "database/sql"

// swagger:model
type Category struct {
	// example: 1
	ID uint `json:"id,omitempty"`
	// example: Кино
	Title string `json:"title"`
}

// swagger:model
type Theme struct {
	// example: 1
	ID uint `json:"id,omitempty"`
	// example: Голливудские фильмы 90-х по кадру
	Title string `json:"title"`
	// example: 1
	CategoryID uint `json:"category_id,omitempty"`
}

// swagger:model
type Image struct {
	// example: 1
	ID uint `json:"id,omitempty"`
	// example: Тестовый url изображения
	Title string `json:"title"`
	// example: http://test_url.com
	ImageUrl string `json:"image_url"`
}

// swagger:model
type Question struct {
	// example: 1
	ID uint `json:"id,omitempty"`
	// example: Когда был снят фильм Forrest Gump?
	Text string `json:"text"`
	// example: 1
	ThemeID uint `json:"theme_id,omitempty"`
	// example: 1
	Img Image `json:"image,omitempty"`
	// example: 1
	RoundID uint `json:"round_id,omitempty"`
}

// swagger:model
type Answer struct {
	// example: 1
	ID uint `json:"id,omitempty"`
	// example: Ответ на вопрос про фильм Forrest Gump
	Title string `json:"title"`
	// example: 1994
	Text string `json:"text"`
	// example: 1
	QuestionID uint `json:"question_id,omitempty"`
	// example: true
	IsCorrect bool `json:"is_correct"`
}

type RoundType struct {
	// example: 1
	ID uint `json:"id,omitempty" xml:"id,omitempty"`
	// example: Скриншоты
	Title string `json:"title" xml:"title"`
	// example: Нужно назвать фильм по скриншоту
	Description string `json:"description"`
}

// swagger:model
type Round struct {
	// example: 1
	ID uint `json:"id,omitempty"`
	// example: 1
	GameId uint `json:"game_id,omitempty"`
	// example: 1
	RoundNumber uint `json:"round_number,omitempty"`
	// example: 1
	RoundTypeID uint        `json:"round_type_id,omitempty"`
	Questions   []*Question `json:"questions,omitempty"`
}

// swagger:model
type GameTheme struct {
	// example: 1
	Id uint `json:"id,omitempty"`
	// example: Кинофантастика #1
	Title string `json:"title,omitempty"`
	// example: Игра про зарубежную и российскую кинофантастику
	Description string `json:"description"`
	// example: 1
	ThemeId uint `json:"theme_id,omitempty"`
}

// swagger:model
type Game struct {
	// example: 1
	ID uint `json:"id,omitempty"`
	// example: Зарубежная кинофантастика
	Title sql.NullString `json:"title,omitempty"`
	// example: Игра про зарубежной кинофантастике
	Description sql.NullString `json:"description"`
	// example: 1
	GameThemeId sql.NullInt64 `json:"game_theme_id,omitempty"`
	Rounds      []Round       `json:"rounds,omitempty"`
}
