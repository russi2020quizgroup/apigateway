package models

import (
	"fmt"
	"net/mail"
)

// ValidationErr represents an error that occurred during validation.
type ValidationErr struct {
	Errs []error
}

// Error method makes ValidationErr satisfy the error interface.
func (ve *ValidationErr) Error() string {
	errStr := "Validation errors occurred: "
	for _, err := range ve.Errs {
		errStr = fmt.Sprintf("%s\n%s", errStr, err.Error())
	}
	return errStr
}

// swagger:model
type RegisterRequest struct {
	// required: true
	// unique: true
	// example: test@test.com
	Email string `json:"email" xml:"email"`
	// required: true
	// example: test_password
	Password string `json:"password" xml:"password"`
}

func (r *RegisterRequest) Validate() error {
	var errs []error
	if r.Email == "" || !isValidEmail(r.Email) {
		errs = append(errs, fmt.Errorf("invalid email"))
	}
	if r.Password == "" {
		errs = append(errs, fmt.Errorf("password cannot be blank"))
	}
	if len(errs) > 0 {
		return &ValidationErr{Errs: errs}
	}
	return nil
}

// swagger:model
type RegisterResponse struct {
	// example: 201
	Status int `json:"status" xml:"status"`
}

func (r *RegisterResponse) Validate() error {
	// Assuming 201 is the only valid status for this response
	if r.Status != 201 {
		return fmt.Errorf("invalid status code: %d", r.Status)
	}
	return nil
}

// swagger:model
type LoginRequest struct {
	// required: true
	// example: test@test.com
	Email string `json:"email" xml:"email"`
	// required: true
	// example: test_password
	Password string `json:"password" xml:"password"`
}

func (l *LoginRequest) Validate() error {
	var errs []error
	if l.Email == "" || !isValidEmail(l.Email) {
		errs = append(errs, fmt.Errorf("invalid email"))
	}
	if l.Password == "" {
		errs = append(errs, fmt.Errorf("password cannot be blank"))
	}
	if len(errs) > 0 {
		return &ValidationErr{Errs: errs}
	}
	return nil
}

// swagger:model
type LoginResponse struct {
	// example: 201
	Status int `json:"status" xml:"status"`
	// example: someklsdjcxskTestlxkncvcllToken
	Token string `json:"token" xml:"token"`
}

func (l *LoginResponse) Validate() error {
	if l.Status != 200 {
		return fmt.Errorf("invalid status code: %d", l.Status)
	}
	if l.Token == "" {
		return fmt.Errorf("token cannot be blank")
	}
	return nil
}

func isValidEmail(email string) bool {
	_, err := mail.ParseAddress(email)
	return err == nil
}
