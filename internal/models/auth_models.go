package models

// swagger:model
type User struct {
	// example: 1
	Id uint `json:"id" xml:"id"`
	// example: test@test.com
	Email string `json:"email" xml:"email"`
	// example: test_password
	Password string `json:"password" xml:"password"`
}
