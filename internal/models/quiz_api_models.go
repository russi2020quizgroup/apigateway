package models

// swagger:model
type CategoriesResponse struct {
	// example: 200
	Status     int         `json:"status"`
	Categories []*Category `json:"category"`
}

// swagger:model
type ThemesResponse struct {
	// example: 200
	Status int      `json:"status"`
	Themes []*Theme `json:"themes"`
}

// swagger:model
type GameThemesResponse struct {
	// example: 200
	Status int          `json:"status"`
	Themes []*GameTheme `json:"game_themes"`
}

// swagger:model
type GameByGameThemeIdResponse struct {
	// example: 200
	Status uint `json:"status"`
	Game   Game `json:"game"`
}
