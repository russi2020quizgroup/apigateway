package api

import (
	"fmt"
	"net/http"
	"strings"
)

// ContentTypeMiddleware устанавливает Content-Type JSON или XML в зависмости от запроса
func (a *API) ContentTypeMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		acceptHeader := r.Header.Get("Accept")
		contentType := "application/json"

		if strings.Contains(acceptHeader, "application/xml") {
			contentType = "application/xml"
		}

		w.Header().Set("Content-Type", contentType)
		next.ServeHTTP(w, r)
	})
}

// InfoLogMiddleware логгирует все запросы
func (a *API) InfoLogMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		reqPath := fmt.Sprintf("%s request to endpoint path %s", r.Method, r.URL.Path)
		a.logger.Info(reqPath)
		next.ServeHTTP(w, r)
	})
}
