package api

import (
	"github.com/gorilla/mux"
	ci "gitlab.com/Russi2020QuizGroup/APIGateway/internal/controller/interfaces"
	li "gitlab.com/Russi2020QuizGroup/APIGateway/pkg/logger/interfaces"
	ri "gitlab.com/Russi2020QuizGroup/APIGateway/pkg/router/interfaces"
	"net/http"
)

// API Программный интерфейс сервера Scheduler
type API struct {
	router     ri.RouterInterface
	controller ci.Controller
	logger     li.Logger
}

func New(controller ci.Controller, logger li.Logger, gatewayRouter ri.RouterInterface) *API {
	api := API{
		router:     gatewayRouter,
		controller: controller,
		logger:     logger,
	}
	api.endpoints()

	return &api
}

// endpoints Регистрация обработчиков API.
func (a *API) endpoints() {
	a.router.Endpoints(a.controller.Routes(), []mux.MiddlewareFunc{
		a.ContentTypeMiddleware, a.InfoLogMiddleware,
	})
}

// Router получаем объект роутера
func (a *API) Router() http.Handler {
	return a.router.Router()
}
