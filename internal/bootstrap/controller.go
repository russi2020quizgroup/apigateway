package bootstrap

import (
	"gitlab.com/Russi2020QuizGroup/APIGateway/internal/controller"
	ci "gitlab.com/Russi2020QuizGroup/APIGateway/internal/controller/interfaces"
	cache "gitlab.com/Russi2020QuizGroup/APIGateway/pkg/cache/interfaces"
	"gitlab.com/Russi2020QuizGroup/APIGateway/pkg/config"
	li "gitlab.com/Russi2020QuizGroup/APIGateway/pkg/logger/interfaces"
	"gitlab.com/Russi2020QuizGroup/APIGateway/pkg/router/types"
)

func provideController(config *config.Config, logger li.Logger, cache cache.CacheStorageInterface, routeMap *types.RouteMap) ci.Controller {
	return controller.New(config.BaseAuthSvcUrl, cache, routeMap, config.RequestTimeout, logger, config.GameAPIUrl)
}

func provideRouteMap() *types.RouteMap {
	return types.New()
}
