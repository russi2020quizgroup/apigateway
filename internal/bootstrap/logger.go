package bootstrap

import (
	"gitlab.com/Russi2020QuizGroup/APIGateway/pkg/config"
	"gitlab.com/Russi2020QuizGroup/APIGateway/pkg/logger"
	li "gitlab.com/Russi2020QuizGroup/APIGateway/pkg/logger/interfaces"
)

func provideLogger(config *config.Config) (li.Logger, error) {
	loggerInstance, err := logger.NewLogger(config.DebugON)
	if err != nil {
		return nil, err
	}
	return loggerInstance, nil
}
