package bootstrap

import (
	"go.uber.org/fx"
)

func Setup() *fx.App {
	return fx.New(
		fx.Provide(
			provideConfig,
			provideLogger,
			provideRouter,
			provideRouteMap,
			provideController,
			provideCache,
			provideAPI,
		),
		fx.Invoke(StartApp),
	)
}
