package bootstrap

import (
	"gitlab.com/Russi2020QuizGroup/APIGateway/pkg/cache"
	ci "gitlab.com/Russi2020QuizGroup/APIGateway/pkg/cache/interfaces"
	"gitlab.com/Russi2020QuizGroup/APIGateway/pkg/config"
	li "gitlab.com/Russi2020QuizGroup/APIGateway/pkg/logger/interfaces"
	"os"
)

func provideCache(config *config.Config, logger li.Logger) (ci.CacheStorageInterface, error) {
	curDir, err := os.Getwd()
	if err != nil {
		logger.Error(err.Error())
		return nil, err
	}

	var redisUrl string
	if curDir == config.DockerPath {
		redisUrl = config.RedisUrlDocker
	} else {
		// Для старта локально без контейнера
		redisUrl = config.RedisUrlLocal
	}

	return cache.NewCache(redisUrl, config.InfoExpirationHours)
}
