package bootstrap

import "gitlab.com/Russi2020QuizGroup/APIGateway/pkg/config"

func provideConfig() (*config.Config, error) {
	return config.LoadConfig()
}
