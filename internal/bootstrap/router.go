package bootstrap

import (
	"gitlab.com/Russi2020QuizGroup/APIGateway/pkg/router"
	ar "gitlab.com/Russi2020QuizGroup/APIGateway/pkg/router/interfaces"
)

func provideRouter() ar.RouterInterface {
	return router.New()
}
