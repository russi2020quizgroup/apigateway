package interfaces

import (
	rt "gitlab.com/Russi2020QuizGroup/APIGateway/pkg/router/types"
	"net/http"
)

type Controller interface {
	Register(w http.ResponseWriter, r *http.Request)
	Login(w http.ResponseWriter, r *http.Request)
	Themes(w http.ResponseWriter, r *http.Request)
	Categories(w http.ResponseWriter, r *http.Request)
	GamesThemes(w http.ResponseWriter, r *http.Request)
	GameByGameThemeIdHandler(w http.ResponseWriter, r *http.Request)
	LogMaker
	RouteUrlProvider
	RouteMethodsProvider
	RequestTimeOutProvider
	RoutesMaker
	RoutesProvider
}

type RequestTimeOutProvider interface {
	RequestTimeout() int
}

type LogMaker interface {
	Log(logType, template string, args ...interface{})
}

type RoutesMaker interface {
	AddRoutes()
}

type RoutesProvider interface {
	Routes() rt.Routes
}

type RouteUrlProvider interface {
	RouteUrl(key string) string
}

type RouteMethodsProvider interface {
	RouteMethods(key string) []string
}
