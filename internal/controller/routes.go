package controller

import (
	rt "gitlab.com/Russi2020QuizGroup/APIGateway/pkg/router/types"
	"net/http"
)

func (c *Controller) AddRoutes() {
	// Auth service routes
	{
		route := rt.Route{
			Name:    "register",
			Pattern: "/register",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.Register(w, r)
			},
			Methods: []string{http.MethodPost, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
		c.RouteMap.AddRouteNames(route.Name, rt.RouteMapInfo{FullPath: c.AuthSvcUrl + route.Pattern, Methods: route.Methods})
	}

	{
		route := rt.Route{
			Name:    "login",
			Pattern: "/login",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.Login(w, r)
			},
			Methods: []string{http.MethodPost, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
		c.RouteMap.AddRouteNames(route.Name, rt.RouteMapInfo{FullPath: c.AuthSvcUrl + route.Pattern, Methods: route.Methods})
	}

	// Quiz service routes
	{
		route := rt.Route{
			Name:    "categories",
			Pattern: "/categories",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.Categories(w, r)
			},
			Methods: []string{http.MethodGet, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
		c.RouteMap.AddRouteNames(route.Name, rt.RouteMapInfo{FullPath: c.GameAPIUrl + route.Pattern, Methods: route.Methods})
	}

	{
		route := rt.Route{
			Name:    "themes",
			Pattern: "/themes",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.Themes(w, r)
			},
			Methods: []string{http.MethodGet, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
		c.RouteMap.AddRouteNames(route.Name, rt.RouteMapInfo{FullPath: c.GameAPIUrl + route.Pattern, Methods: route.Methods})
	}

	{
		route := rt.Route{
			Name:    "game_themes",
			Pattern: "/game-theme",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.GamesThemes(w, r)
			},
			Methods: []string{http.MethodGet, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
		c.RouteMap.AddRouteNames(route.Name, rt.RouteMapInfo{FullPath: c.GameAPIUrl + route.Pattern, Methods: route.Methods})
	}

	{
		route := rt.Route{
			Name:    "game_by_game_theme_id",
			Pattern: "/game/game_theme",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.GameByGameThemeIdHandler(w, r)
			},
			Methods: []string{http.MethodGet, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
		c.RouteMap.AddRouteNames(route.Name, rt.RouteMapInfo{FullPath: c.GameAPIUrl + route.Pattern, Methods: route.Methods})
	}
}

func (c *Controller) Routes() rt.Routes {
	return c.routes
}
