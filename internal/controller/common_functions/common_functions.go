package common_functions

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	el "gitlab.com/Russi2020QuizGroup/APIGateway/internal/controller/error_levels"
	ci "gitlab.com/Russi2020QuizGroup/APIGateway/internal/controller/interfaces"
	apiModels "gitlab.com/Russi2020QuizGroup/APIGateway/internal/models"
	"io"
	"net"
	"net/http"
	"net/url"
	"strings"
	"time"
)

const (
	register          = "register"
	login             = "login"
	categories        = "categories"
	themes            = "themes"
	gameThemes        = "game_themes"
	gameByGameThemeId = "game_by_game_theme_id"
)

const MaxRetries = 3

func PerformAction(action string, controller ci.Controller, w http.ResponseWriter, r *http.Request) (*http.Response, bool) {
	routeURL := controller.RouteUrl(action)
	method := controller.RouteMethods(action)
	body := new(bytes.Buffer)
	bodyBytes, err := io.ReadAll(r.Body)

	if err != nil {
		controller.Log(el.ErrorF, "Couldn't read body of request. Got error %s", err.Error())
		http.Error(w, "An error occurred. Please try again later.", http.StatusInternalServerError)
		return nil, false
	}
	r.Body.Close()
	r.Body = io.NopCloser(bytes.NewBuffer(bodyBytes))

	if err != nil {
		controller.Log(el.ErrorF, "Couldn't read body of request. Got error %s", err.Error())
		http.Error(w, "An error occurred. Please try again later.", http.StatusInternalServerError)
		return nil, false
	}

	if r.URL.RawQuery != "" {
		routeURL = routeURL + "?" + r.URL.RawQuery
	}

	if r.Body != nil {
		requestModel, err := RequestModel(bodyBytes, action)

		if err != nil {
			controller.Log(el.ErrorF, "Couldn't unmarshal body of request. Got error %s", err.Error())
			http.Error(w, "An error occurred. Please try again later.", http.StatusInternalServerError)
			return nil, false
		}

		if err := json.NewEncoder(body).Encode(requestModel); err != nil {
			controller.Log(el.ErrorF, "Couldn't encode body of request. Got error %s", err.Error())
			http.Error(w, "An error occurred. Please try again later.", http.StatusInternalServerError)
			return nil, false
		}
	}

	if routeURL == "" {
		controller.Log(el.ErrorF, "Error creating HTTP request. Route %s doesn't exist", routeURL)
		http.Error(w, "An error occurred. Please try again later.", http.StatusInternalServerError)
		return nil, false
	}

	var resp *http.Response

	// Create a new context
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(controller.RequestTimeout())*time.Second)
	defer cancel() // Cancel context if it is still alive when the function returns

	for i := 0; i < MaxRetries; i++ {
		controller.Log(el.InfoF, "Request number %d\n", i+1)
		req, err := http.NewRequestWithContext(ctx, method[0], routeURL, bytes.NewBuffer(bodyBytes))
		if err != nil {
			controller.Log(el.ErrorF, "Error creating HTTP request. Got error %s", err)
			http.Error(w, "An error occurred. Please try again later.", http.StatusInternalServerError)
			return nil, false
		}
		req.Header.Add("Content-Type", "application/json")

		c := &http.Client{}

		resp, err = c.Do(req)
		if err == nil {
			return resp, true // no error, return the response
		}

		// If error is a timeout error, log it and try again
		if nErr, ok := err.(net.Error); ok && nErr.Timeout() {
			controller.Log(el.WarnF, "Attempt %d encountered a timeout error: %v", i+1, err)
			time.Sleep(time.Second * 2)
			continue
		}

		if strings.Contains(err.Error(), "EOF") {
			controller.Log(el.ErrorF, "Got an error: %v", err)
			HandleError(err, w, controller, fmt.Sprintf("Couldn't send %s request. Got error %%s", action))
			return nil, false
		}

		// For other errors, stop and handle the error
		break
	}

	HandleError(err, w, controller, fmt.Sprintf("Couldn't send %s request. Got error %%s", action))
	return nil, false
}

func HandleError(err error, w http.ResponseWriter, controller ci.Controller, message string) {
	if err == nil {
		controller.Log(el.WarnF, "Got error with nil value")
		return
	}
	switch e := err.(type) {
	case *url.Error:
		if opErr, ok := e.Err.(*net.OpError); ok {
			if opErr.Timeout() {
				controller.Log(el.WarnF, "Request timeout: %s", err)
				http.Error(w, "Request timed out. Please try again later.", http.StatusRequestTimeout)
				return
			}
		}
		if errors.Is(err, context.DeadlineExceeded) {
			controller.Log(el.WarnF, "Context deadline exceeded: %s", err)
			http.Error(w, "Request exceeded time limit", http.StatusRequestTimeout)
			return
		}
		if errors.Is(err, context.Canceled) {
			controller.Log(el.WarnF, "Request was cancelled: %s", err)
			http.Error(w, "Request was cancelled by user", http.StatusBadRequest)
			return
		}
		controller.Log(el.ErrorF, message, e)
		http.Error(w, "Invalid URL: "+e.Error(), http.StatusBadRequest)
	case *apiModels.ValidationErr:
		controller.Log(el.WarnF, "Validation error: %s", err)
		http.Error(w, "Invalid input dat s. Please check and try again.", http.StatusBadRequest)
	case net.Error:
		if e.Timeout() {
			controller.Log(el.WarnF, "Request timeout: %s", err)
			http.Error(w, "Request timed out. Please try again later.", http.StatusRequestTimeout)
		} else {
			controller.Log(el.ErrorF, message, e)
			http.Error(w, "An error occurred. Please try again later.", http.StatusInternalServerError)
		}
	case *json.SyntaxError:
		controller.Log(el.ErrorF, message, e)
		http.Error(w, "Invalid JSON: "+e.Error(), http.StatusBadRequest)
	case *json.UnmarshalTypeError:
		controller.Log(el.ErrorF, message, e)
		http.Error(w, "Invalid JSON: "+e.Error(), http.StatusBadRequest)
	case *json.UnsupportedTypeError:
		controller.Log(el.ErrorF, message, e)
		http.Error(w, "Invalid JSON: "+e.Error(), http.StatusBadRequest)
	case *json.UnsupportedValueError:
		controller.Log(el.ErrorF, message, e)
		http.Error(w, "Invalid JSON: "+e.Error(), http.StatusBadRequest)
	case *json.InvalidUnmarshalError:
		controller.Log(el.ErrorF, message, e)
		http.Error(w, "Invalid JSON: "+e.Error(), http.StatusBadRequest)
	case *json.MarshalerError:
		controller.Log(el.ErrorF, message, e)
		http.Error(w, "Invalid JSON: "+e.Error(), http.StatusBadRequest)
	default:
		// Default case when error doesn't match any expected types
		if err == nil {
			controller.Log(el.ErrorF, message, err)
			http.Error(w, "An error occurred. Please try again later.", http.StatusInternalServerError)
			return
		}
		// Check if the error is "E-Mail already exists"
		if err.Error() == "E-Mail already exists" {
			controller.Log(el.ErrorF, message, err)
			http.Error(w, "The provided E-Mail already exists. Please use a different E-Mail.", http.StatusConflict)
			return
		}

		controller.Log(el.ErrorF, message, err)
		http.Error(w, "An error occurred. Please try again later.", http.StatusInternalServerError)
	}
}

func SendResponse(data interface{}, w http.ResponseWriter, controller ci.Controller) {
	if err := json.NewEncoder(w).Encode(data); err != nil {
		controller.Log(el.ErrorF, "Couldn't encode body of response. Got error %s", err.Error())
		http.Error(w, "An error occurred. Please try again later.", http.StatusInternalServerError)
	}
}

func RequestModel(data []byte, action string) (interface{}, error) {
	var requestModel interface{}

	switch action {
	case login:
		var loginRequest apiModels.LoginRequest
		if err := json.Unmarshal(data, &loginRequest); err != nil {
			return nil, err
		}
		requestModel = loginRequest
	case register:
		var registerRequest apiModels.RegisterRequest
		if err := json.Unmarshal(data, &registerRequest); err != nil {
			return nil, err
		}
		requestModel = registerRequest
	}

	return requestModel, nil
}
