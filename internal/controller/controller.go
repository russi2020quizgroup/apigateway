package controller

import (
	"bytes"
	"encoding/json"
	"errors"
	cf "gitlab.com/Russi2020QuizGroup/APIGateway/internal/controller/common_functions"
	el "gitlab.com/Russi2020QuizGroup/APIGateway/internal/controller/error_levels"
	ci "gitlab.com/Russi2020QuizGroup/APIGateway/internal/controller/interfaces"
	apiModels "gitlab.com/Russi2020QuizGroup/APIGateway/internal/models"
	cacheInterface "gitlab.com/Russi2020QuizGroup/APIGateway/pkg/cache/interfaces"
	li "gitlab.com/Russi2020QuizGroup/APIGateway/pkg/logger/interfaces"
	rt "gitlab.com/Russi2020QuizGroup/APIGateway/pkg/router/types"
	"io"
	"net/http"
	"strings"
)

var _ ci.Controller = (*Controller)(nil)

const (
	register          = "register"
	login             = "login"
	categories        = "categories"
	themes            = "themes"
	gameThemes        = "game_themes"
	gameByGameThemeId = "game_by_game_theme_id"
)

type Controller struct {
	AuthSvcUrl     string
	Cache          cacheInterface.CacheStorageInterface
	Logger         li.Logger
	requestTimeOut int
	routes         rt.Routes
	RouteMap       *rt.RouteMap
	GameAPIUrl     string
}

func New(
	authUrl string,
	cache cacheInterface.CacheStorageInterface,
	routeMap *rt.RouteMap,
	rt int,
	logger li.Logger,
	gameApiUrl string,
) *Controller {
	controller := &Controller{
		AuthSvcUrl:     authUrl,
		Cache:          cache,
		Logger:         logger,
		requestTimeOut: rt,
		RouteMap:       routeMap,
		GameAPIUrl:     gameApiUrl,
	}
	controller.AddRoutes()
	return controller
}

func (c *Controller) RequestTimeout() int {
	return c.requestTimeOut
}

func (c *Controller) Log(logType, template string, args ...interface{}) {
	switch logType {
	case el.ErrorF:
		c.Logger.Errorf(template, args)
	case el.InfoF:
		c.Logger.Infof(template, args)
	case el.WarnF:
		c.Logger.Warnf(template, args)
	}
}

func (c *Controller) RouteUrl(key string) string {
	return c.RouteMap.RouteUrl(key)
}

func (c *Controller) RouteMethods(key string) []string {
	return c.RouteMap.RouteMethods(key)
}

// Register registers a new user
func (c *Controller) Register(w http.ResponseWriter, r *http.Request) {
	// Create a new reader for Decode (this one will be drained after Decode)
	bodyReaderForDecode := newReader(r, c)

	var req apiModels.RegisterRequest
	err := json.NewDecoder(bodyReaderForDecode).Decode(&req)
	if err != nil {
		c.Logger.Errorf("Couldn't parse register request. Got error %s", err.Error())
		return
	}

	err = req.Validate()
	if err != nil {
		c.Logger.Errorf("Invalid register request. Got error %s", err.Error())
		cf.HandleError(err, w, c, "Invalid login request. Got error %s")
		return
	}

	resp, ok := cf.PerformAction(register, c, w, r)
	if !ok {
		return
	}

	processRegisterResponse(resp, w, c)
}

// Login authenticates a user and returns a token if successful
func (c *Controller) Login(w http.ResponseWriter, r *http.Request) {
	// Create a new reader for Decode (this one will be drained after Decode)
	bodyReaderForDecode := newReader(r, c)

	var req apiModels.LoginRequest
	err := json.NewDecoder(bodyReaderForDecode).Decode(&req)
	if err != nil {
		c.Logger.Errorf("Couldn't parse login request. Got error %s", err.Error())
		return
	}

	// Validate req
	err = req.Validate()
	if err != nil {
		c.Logger.Errorf("Invalid login request. Got error %s", err.Error())
		cf.HandleError(err, w, c, "Invalid login request. Got error %s")
		return
	}

	resp, ok := cf.PerformAction(login, c, w, r) // Now r.Body can be read correctly in PerformAction()
	if !ok {
		return
	}

	processLoginResponse(resp, w, c)
}

func (c *Controller) Themes(w http.ResponseWriter, r *http.Request) {
	resp, ok := cf.PerformAction(themes, c, w, r)
	if !ok {
		c.Logger.Errorf("Couldn't perform action %s", themes)
		return
	}

	processThemesResponse(resp, w, c)
}

func (c *Controller) Categories(w http.ResponseWriter, r *http.Request) {
	resp, ok := cf.PerformAction(categories, c, w, r)
	if !ok {
		c.Logger.Errorf("Couldn't perform action %s", categories)
		return
	}

	processCategoriesResponse(resp, w, c)
}

func (c *Controller) GamesThemes(w http.ResponseWriter, r *http.Request) {
	resp, ok := cf.PerformAction(gameThemes, c, w, r)
	if !ok {
		c.Logger.Errorf("Couldn't perform action %s", gameThemes)
		return
	}

	processGameThemesResponse(resp, w, c)
}

func (c *Controller) GameByGameThemeIdHandler(w http.ResponseWriter, r *http.Request) {
	resp, ok := cf.PerformAction(gameByGameThemeId, c, w, r)
	if !ok {
		c.Logger.Errorf("Couldn't perform action %s", gameByGameThemeId)
		return
	}

	processGameByGameThemeIdResponse(resp, w, c)
}

func processRegisterResponse(resp *http.Response, w http.ResponseWriter, c *Controller) {
	info, err := io.ReadAll(resp.Body)

	// Check if returned string equals 'E-Mail already exists'
	if err == nil && strings.TrimSpace(string(info)) == "E-Mail already exists" {
		err = errors.New("E-Mail already exists") // Create a new error with the message
	}

	if err != nil {
		cf.HandleError(err, w, c, "Couldn't read or process body of register response. Got error %s")
		return
	}

	var registerResponse apiModels.RegisterResponse
	err = json.Unmarshal(info, &registerResponse)
	if err != nil {
		cf.HandleError(err, w, c, "Couldn't unmarshal body of register response. Got error %s")
		return
	}

	// Validate the response
	err = registerResponse.Validate()
	if err != nil {
		cf.HandleError(err, w, c, "Invalid register response. Got error %s")
		return
	}

	cf.SendResponse(registerResponse, w, c)
}

func processLoginResponse(resp *http.Response, w http.ResponseWriter, c *Controller) {
	info, err := io.ReadAll(resp.Body)
	if err != nil {
		cf.HandleError(err, w, c, "Couldn't read body of login response. Got error %s")
		return
	}

	var loginResponse apiModels.LoginResponse
	err = json.Unmarshal(info, &loginResponse)
	if err != nil {
		cf.HandleError(err, w, c, "Couldn't unmarshal body of login response. Got error %s")
		return
	}

	// Validate response
	err = loginResponse.Validate()
	if err != nil {
		c.Logger.Errorf("Invalid login response. Got error %s", err.Error())
		// Optionally send an error response to the user here
		return
	}

	cf.SendResponse(loginResponse, w, c)
}

func processThemesResponse(resp *http.Response, w http.ResponseWriter, c *Controller) {
	info, err := io.ReadAll(resp.Body)
	if err != nil {
		cf.HandleError(err, w, c, "Couldn't read body of themes response. Got error %s")
		return
	}

	var themesResponse apiModels.ThemesResponse
	err = json.Unmarshal(info, &themesResponse)
	if err != nil {
		cf.HandleError(err, w, c, "Couldn't unmarshal body of themes response. Got error %s")
		return
	}

	cf.SendResponse(themesResponse, w, c)
}

func processCategoriesResponse(resp *http.Response, w http.ResponseWriter, c *Controller) {
	info, err := io.ReadAll(resp.Body)
	if err != nil {
		cf.HandleError(err, w, c, "Couldn't read body of categories response. Got error %s")
		return
	}

	var categoriesResponse apiModels.CategoriesResponse
	err = json.Unmarshal(info, &categoriesResponse)
	if err != nil {
		cf.HandleError(err, w, c, "Couldn't unmarshal body of categories response. Got error %s")
		return
	}

	cf.SendResponse(categoriesResponse, w, c)
}

func processGameThemesResponse(resp *http.Response, w http.ResponseWriter, c *Controller) {
	info, err := io.ReadAll(resp.Body)
	if err != nil {
		cf.HandleError(err, w, c, "Couldn't read body of game themes response. Got error %s")
		return
	}

	var gameThemesResponse apiModels.GameThemesResponse
	err = json.Unmarshal(info, &gameThemesResponse)
	if err != nil {
		cf.HandleError(err, w, c, "Couldn't unmarshal body of game themes response. Got error %s")
		return
	}

	cf.SendResponse(gameThemesResponse, w, c)
}

func processGameByGameThemeIdResponse(resp *http.Response, w http.ResponseWriter, c *Controller) {
	info, err := io.ReadAll(resp.Body)
	if err != nil {
		cf.HandleError(err, w, c, "Couldn't read body of game by game theme id response. Got error %s")
		return
	}

	var gameByGameThemeIdResponse apiModels.GameByGameThemeIdResponse
	err = json.Unmarshal(info, &gameByGameThemeIdResponse)
	if err != nil {
		cf.HandleError(err, w, c, "Couldn't unmarshal body of game by game theme id response. Got error %s")
		return
	}

	cf.SendResponse(gameByGameThemeIdResponse, w, c)
}

func newReader(r *http.Request, c *Controller) *bytes.Reader {
	// Read body to byte array
	bodyBytes, err := io.ReadAll(r.Body)
	if err != nil {
		c.Logger.Errorf("Couldn't read body of login request. Got error %s", err.Error())
		return nil
	}
	// Replace original r.Body with a reader for later usage (NOT for Decode, which drains the reader)
	r.Body = io.NopCloser(bytes.NewReader(bodyBytes))

	// Create a new reader for Decode (this one will be drained after Decode)
	bodyReaderForDecode := bytes.NewReader(bodyBytes)

	return bodyReaderForDecode
}
