package main

import "gitlab.com/Russi2020QuizGroup/APIGateway/internal/bootstrap"

func main() {
	app := bootstrap.Setup()
	app.Run()
}
