package cache

import (
	"fmt"
	"github.com/go-redis/redis"
	"gitlab.com/Russi2020QuizGroup/APIGateway/pkg/cache/interfaces"
	"time"
)

var _ interfaces.CacheStorageInterface = (*Cache)(nil)

type Cache struct {
	Client          *redis.Client
	ExpirationHours int64
}

func NewCache(redisUrl string, infoExpireHours int64) (*Cache, error) {
	client := redis.NewClient(&redis.Options{
		Addr:     redisUrl,
		Password: "", // Omit if no password is set
		DB:       0,  // Use default DB
	})

	_, err := client.Ping().Result()
	if err != nil {
		return nil, err
	}

	return &Cache{
		Client:          client,
		ExpirationHours: infoExpireHours,
	}, nil
}

func (uts *Cache) Set(key string, value string) error {
	expirationTime := time.Duration(uts.ExpirationHours) * time.Hour
	err := uts.Client.Set(key, value, expirationTime).Err()
	if err != nil {
		return err
	}

	return nil
}

func (uts *Cache) Get(key string) (string, error) {
	token, err := uts.Client.Get(key).Result()
	if err == redis.Nil {
		return "", fmt.Errorf("value not found")
	} else if err != nil {
		return "", err
	}

	return token, nil
}
