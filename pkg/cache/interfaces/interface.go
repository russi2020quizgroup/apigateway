package interfaces

type CacheStorageInterface interface {
	Set(key string, value string) error
	Get(key string) (string, error)
}
