package cache

import (
	"github.com/alicebob/miniredis"
	"github.com/go-redis/redis"
)

var redisServer *miniredis.Miniredis

func mockRedis() *miniredis.Miniredis {
	s, err := miniredis.Run()

	if err != nil {
		panic(err)
	}

	return s
}

func MockUserTokenStorage() *Cache {
	mockStorage := &Cache{}
	redisServer = mockRedis()
	mockStorage.Client = redis.NewClient(&redis.Options{
		Addr: redisServer.Addr(),
	})
	return mockStorage
}

func Teardown() {
	redisServer.Close()
}
