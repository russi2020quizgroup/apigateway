package types

import "net/http"

type Routes []Route

type Route struct {
	Name        string
	Methods     []string
	Pattern     string
	HandlerFunc func(http.ResponseWriter, *http.Request)
}

type RouteMapInfo struct {
	FullPath string
	Methods  []string
}

type RouteMap struct {
	routeMapInstance map[string]RouteMapInfo
}

func New() *RouteMap {
	rm := &RouteMap{
		routeMapInstance: make(map[string]RouteMapInfo),
	}
	return rm
}

func (rm *RouteMap) RouteUrl(key string) string {
	return rm.routeMapInstance[key].FullPath
}

func (rm *RouteMap) RouteMethods(key string) []string {
	return rm.routeMapInstance[key].Methods
}

func (rm *RouteMap) AddRouteNames(key string, routeInfo RouteMapInfo) {
	rm.routeMapInstance[key] = routeInfo
}
