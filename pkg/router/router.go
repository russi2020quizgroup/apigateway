package router

import (
	"github.com/gorilla/mux"
	sri "gitlab.com/Russi2020QuizGroup/APIGateway/pkg/router/interfaces"
	sr "gitlab.com/Russi2020QuizGroup/APIGateway/pkg/router/types"
	"net/http"
)

var _ sri.RouterInterface = (*ApiGatewayRouter)(nil)

type ApiGatewayRouter struct {
	router *mux.Router
}

// New создает новый инстанс роутера
func New() *ApiGatewayRouter {
	return &ApiGatewayRouter{
		router: mux.NewRouter(),
	}
}

// Router получает роутер
func (qr *ApiGatewayRouter) Router() http.Handler {
	return qr.router
}

// Endpoints endpoints Регистрация обработчиков API.
func (qr *ApiGatewayRouter) Endpoints(routes sr.Routes, middleware []mux.MiddlewareFunc) {
	qr.MakeHandleFunctions(routes, middleware)
}

// MakeHandleFunctions создает хендлеры из рутов Route
func (qr *ApiGatewayRouter) MakeHandleFunctions(routes sr.Routes, middleware []mux.MiddlewareFunc) {
	qr.router.Use(middleware...)
	for _, route := range routes {
		qr.router.HandleFunc(route.Pattern, route.HandlerFunc).Methods(route.Methods...)
	}
}
