package config

import (
	"github.com/joho/godotenv"
	"github.com/spf13/viper"
	"log"
	"os"
)

type Config struct {
	BaseAPIUrl          string `mapstructure:"BASE_API_URL"`
	BaseAuthSvcUrl      string `mapstructure:"BASE_AUTH_SVC_URL"`
	DebugON             bool   `mapstructure:"DEBUG_ON"`
	GameAPIUrl          string `mapstructure:"QUIZ_GAME_URL"`
	Port                string `mapstructure:"PORT"`
	DBUrl               string `mapstructure:"DB_URL"`
	DBUrlDocker         string `mapstructure:"DB_URL_DOCKER"`
	RequestTimeout      int    `mapstructure:"REQUEST_TIMEOUT"`
	RedisUrlLocal       string `mapstructure:"REDIS_URL_LOCAL"`
	RedisUrlDocker      string `mapstructure:"REDIS_URL_DOCKER"`
	InfoExpirationHours int64  `mapstructure:"INFO_EXPIRE_HOURS"`
	DockerPath          string
}

func LoadConfig() (*Config, error) {
	var c Config

	e := godotenv.Load(".env") //Загрузить файл .env из корня приложения
	if e != nil {
		log.Println(e)
	}

	dockerPath := os.Getenv("APP_PATH")
	c.DockerPath = dockerPath

	viper.AddConfigPath("./config/envs")     // Файлы окружения для приложения
	viper.AddConfigPath(dockerPath)          // Путь для приложения в docker контейнере
	viper.AddConfigPath("../../config/envs") // Для тестов

	viper.SetConfigName("prod")
	viper.SetConfigType("env")

	viper.AutomaticEnv()

	err := viper.ReadInConfig()

	if err != nil {
		viper.SetConfigName("dev")
		viper.SetConfigType("env")
		viper.AutomaticEnv()

		err = viper.ReadInConfig()

		if err != nil {
			return nil, err
		}
		err = viper.Unmarshal(&c)

		return &c, nil
	}

	err = viper.Unmarshal(&c)

	if err != nil {
		return nil, err
	}

	return &c, nil
}
