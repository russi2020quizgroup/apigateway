package interfaces

type Logger interface {
	Debug(msg string)
	Info(msg string)
	Infof(template string, args ...interface{})
	Warn(msg string)
	Error(msg string)
	Errorf(template string, args ...interface{})
	Warnf(template string, args ...interface{})
}
